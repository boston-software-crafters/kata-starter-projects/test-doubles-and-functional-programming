# Test Doubles and Functional Programming

We cover the practice of writing Tests for Software very frequently here at [Boston Software Crafters](), but usually apply Test Driven Development to fairly simple cases, some of which are already (intentionally, even!) easy to write tests for.

Unit Testing, especially, is a "functional" practice, and it's not really a surprise that it's easier to do against "purely functional" code: In pure (or "mostly pure") functional scenarios, it's easy to write tests - Pass in some inputs, and make assertions about the expected outputs.

Unfortunately, we often have to deal with things that don't fit a Pure Functional model: Shared state, input/output, and other side effects may be necessary and unavoidable...

How can we change our tactics in difficult situations? What can we do when our code relies on other code or libraries that we can't control, or gets data from dynamic and/or unreliable sources, e.g. "The Internets"?

Let's do a quick, introductory overview of two approaches for Testing things that are hard to test.

_*DISCLAIMER:* These are very deep topics, and we're going to cover two of them here - Needless to say, we're just going to scrape the surface, but hopefully enough to get you interested. Functional Programming, especially, is a "whole thing", but I'm just going to touch on it here at a high level, in the context of testing._

## Setup

At minimum, we need to install the `PyTest` library (a framework for creating, discovering, and running Tests), and `Toolz`, which provides some Functional Programming abstractions for Python. We've also included some other handy Python development tools like `black` (for automatic code formatting), `isort` (for automatic import sorting) and `rope` (which helps VSCode perform refactoring in Python).

_(This setup should happen automatically in GitPod, but if not, use the "Manual Setup via Pip" instructions below.)_

### Automated Setup via Poetry

The dependencies and virtual environment of this project can be managed automatically using `poetry`:
```
poetry install
```

### Manual Setup via Pip
Alternately, create or select a virtual environment through some other means, then install the required packages directly using `pip`. _(If you open this repo in GitPod, you should get a suitable Python environment automatically.)_
```
pip install -r requirements.txt
```

## The Exercises

Good tests evaluate a unit of code in isolation - It's a simple idea, but can be hard to apply, especially in existing codebases, when working with other libraries or frameworks, and especially in apps that need to communciate over the internet.

Here are two general approaches to make our lives easier, and help us to write better Tests:

## Test Doubles (and better Mocks)

Real life code may not be isolated - We often need to test code that reads from or writes to files, communicates over networks, or waits for time to pass - But we want our Tests to run quickly, and not depend on external resources.

This is where Test Doubles (also known as Mocks) come in: Allowing us to replace a real code and resources with "fake" ones, essentially creating a reliable testing context "around" the code that we're trying to test. There are many different kinds of Test Doubles, suitable for different situations, and a few of them have earned a bad reputation for being overly complicated, and even problematic.

Some programmers would tell you that Test Doubles should be avoided if possible, and they're not wrong! But since they're sometimes necessary, it's good to know your options. We'll cover the general types of Test Doubles, when we might consider using them, and what makes those use cases "bad" or "good", so we can hopefully steer more towards the "good" (or at least, "less problematic") side, when we do need to use them.

* [Test Doubles and "Good Mocks"](mocking/mocking.md)

## (A Little) Functional Programming

We mentioned that Test Doubles can be problematic, and it's better to avoid them if possible: So, how *do* we avoid them? Can we be proactive, and write code that is inherently easier to test?

Functional Programming is a _whole thing_, and we can't really do it justice here. But it's worth learning about some of the principles and philosophy, as it relates to making code "more testable" - Code that is inherently easier to Test, with less need for Mocks or Test Doubles.

We'll start by understanding what it means to be "functional", how code can be more "purely functional", and what kinds of "side effects" we want to avoid. With this in mind, we can apply a pattern called "Functional Code, Imperative Shell", and consider how it changes the overall "testability" of our code.

While we're on the subject, we can look at some other Functional Programming tools, such as Python's [functools](https://docs.python.org/3/library/functools.html) module, the newer and more intensive [toolz](https://toolz.readthedocs.io/en/latest/api.html) library, which can help us "trick code into being more functional".

And we can skim some deeper functional concepts, such as "Monads" and the "Maybe" construct (as implemented in the [returns](https://github.com/dry-python/returns) library) - If it's more functional to return a single result, of a known type, how do we deal with Exceptions, DivisionByZero, network timeouts, and other weirdness?

* [Functional Programming (in a Testing Context)](functional/functional.md)
