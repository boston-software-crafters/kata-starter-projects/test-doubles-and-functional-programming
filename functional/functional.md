# Functional Programming

Functional Programming is a very deep, diverse, and sometimes controversial subject, and some smart, very passionate people would be happy to explain why you should always use it - Or never ever use it at all.

To be honest, I'm not an expert, and we don't have the time to cover it in detail today.

But even without going into a lot of detail, there are some ideas we can borrow, in trying to write code that is easier to Test. (It also can't fully eliminate our need for Mocks and other Test Doubles, which is why I covered those first - But it can help us keep their usage to a minimum.)

We've mentioned that Unit Testing is a "functional" practice, but what does that mean? There are some attributes we can use to identify it, in terms of code code.

A function is "pure" (in the sense of being purely functional) if, given the same inputs, it always produces the same outputs, and does not cause any additional side effects.

To be more specific, a pure function:

 * Takes explicit inputs (fewer is better!) and doesn't read from any shared or global state

    * _An ideal function is "monadic", in that it takes a single argument, and returns a single response (often of a similar or "wrapped" type to the input)_

 * Does not write to or otherwise "mutate" any shared or global state _(has no side effects)_

 * Does not "mutate" or alter any object(s) that are passed in as inputs

     * _In practice, this means they returns new objects or values as outputs - treating the input as if it was immutable, even if it *technically* isn't..._

In pure (or "mostly pure") functional scenarios, it's easy to write tests - Pass in some inputs, and make assertions about the expected outputs. Tests don't require much setup, and can be implemented as lists of inputs and their expected outputs. 

Unfortunately, we sometimes have to deal with "side effects" - We might have very practical reasons for breaking the above rules, such as:

 * Reading from or writing to a Database, a File, or some other Data Store (_input/output_, or _I/O_ ), which may take time, and may fail under certain conditions

 * Requesting data from an API, somewhere else in our system (possibly via code we didn't write!)

 * Requesting data through HTTP, which could take time, could produce various results, and where any number of surprising things could go wrong...

## Functional Core Imperative Shell

While "pure functions" are ideal, it's not practical to demand that everything work that way - Side effects and impure functions are necessary, arguably even "the point" of many applications, and thus inevitable. What can we do about this?

Put simply, we should try to keep the "pure functional" code separate from the "impure" code that has side effects: This general approach is often referred to as **"Functional Core, Imperative Shell"**:

![Functional Core, Imperative Shell diagram](https://kennethlange.com/wp-content/uploads/2021/03/functional_core_imperative_shell.png)

_(from https://kennethlange.com/functional-core-imperative-shell )_

One of the simplest ways to achieve this is to move the side effects to the "edges" of your code, and try to keep the "middle" functional. Likewise, while you can call into your "pure" code from the edges, your "pure" functional code should only be returning results (possibly by calling other "pure" code), and should never be calling any code from "impure" section. 

* 

## Making Functions "More Functional"

An ideal function has a single input (of a known type), and a single output (also of a known type), and no side effects. But many functions are not ideal, even within the core libraries of most languages. What can we do about this?

### Wrapping functions (and Partials)

Python makes it fairly easy to "wrap" a function inside another function - We can take an interface that we don't like, and enclose it inside a function with an interface we prefer.

The `functools` module even has a helper function that can do this without writing much code, called `partial`.

We can expose the interface we want, ideally with as few arguments as possible, and the rest of the arguments we don't want or need can be given default values. (This may be implemented as simply "hiding" the extra arguments, if they're already optional, or hardcoding them to specific values. This reduces the flexibility of the original function, but allows you to expose "just enough" for your use case.)

### Pipelines and "Railroads"

Once you've reduced 

### Optional parameters and the Maybe Construct

Another problem with being Functional is that our functions may not work - they ideally return a result of an expected type, but in some cases, they may not return anything (`None`, or `null`), and they may completely fail to run and raise an exception of some kind.

While we can try to avoid a lot of  

The Toolz library provides some Functional constructs that can help with this, though they're not unique to Toolz (or Python) - You'll see very similar concepts in other Functional Programming tools.

### Results, Success and Failure

You can take this even further...