from functional.internals.business_object import BusinessObject
from functional.internals.web import (
    build_api_list_request,
    extract_list_objects,
    extract_list_more_pages,
    build_api_object_request,
    extract_object,
)
from functional.internals.config import load_credentials
from functional.internals.database import db, build_upsert_query

# The Internals folder contains some simulated business logic:
# You should not need to modify any of it!

credentials = load_credentials()
api_list_request = build_api_list_request(credentials)


def update_from_api():
    """
    This code is fairly reasonable, and "works" (at least, as a simulation)
    But it combines pure and impure functionality in a single, monolithic unit of code.

    How can we refactor it to comply with Functional Core, Imperative Shell?
    """
    page = 1
    upsert_count = 0
    error_count = 0

    more_pages = True

    while more_pages:
        api_list_request = build_api_list_request(credentials, page)

        list_response = api_list_request.get()
        raw_object_list = extract_list_objects(list_response)

        for raw_list_item in raw_object_list:

            raw_object_response = build_api_object_request(
                credentials, raw_list_item.id
            )
            raw_object = extract_object(raw_object_response)

            business_object = BusinessObject()
            business_object.init_from_raw(raw_object)

            try:
                business_object.validate()
            except Exception as exp:
                print(
                    "Skipping Invalid Object: ",
                    business_object,
                    " due to exception:",
                    exp,
                )
                error_count += 1
            else:
                upsert_query = build_upsert_query(business_object)
                db.execute(upsert_query)
                upsert_count += 1

        more_pages = extract_list_more_pages(list_response)

    print("Done! Processed {page} page(s), upserted {object_count} object(s).")
