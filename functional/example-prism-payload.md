{
    claims: [employee,employee-developer,employee-developer-assessment-learner,employee-developer-assessment-tools,employee-developer-blanketsburg,employee-developer-ple,employee-sales-business-development],
    hasExternalMentoring:false,
    hasInternalMentoring:true,
    libraries: [118694ca-28f5-4236-8cb3-1e08f5912c39,1c056b1d-a8e3-4c17-8a2b-c8c58676976f,40d2aa62-7c33-47d7-bd06-ba55f6de23d4,4505f4aa-02d9-4d79-9b41-5424223cdae0,74bbd9bf-2ad9-40ba-a108-9545525a0bbf,8b01ea91-a344-494b-b26d-7e60fe8dda51,9c85494b-7f73-4d95-92af-2cf80f01bc5f,a0384327-7058-4e43-82ec-4bf63afb84f8,b1b2a945-ca6a-4c64-9ab3-c89c757745e9,b75abd0c-d1b9-401d-b2e5-5f469b40c5bf,baf67fb6-13fc-4656-ac14-4bc5485174dc,free,d3e22fba-fc02-481f-8ebc-1df40890109d,f9c089d2-ce0d-44df-b2f4-62fe1283d8de],
    planAuthz: {
        userHandle:d7d7b034-0035-4c14-8a64-030a0aa3f005,
        plans:[],
        allPlansAuthz:{
            canViewDashboard:true,
            canViewAccount:true,
            canViewPeople:true,
            canViewAnalytics:true,
            canViewLogs:true,
            canViewSettings:false,
            canViewAllUsers:true,
            canViewBilling:false,
            canInviteUsers:false,
            canViewPriorities:true,
            canViewCustomRoleIq:false
        },
        cacheUntil: 2023-01-11T17:03:08.5647678+00:00
    },
    planId: pluralsight-engineering-1d36a,
    userHandle: d7d7b034-0035-4c14-8a64-030a0aa3f005,
    isPrismSuppressed: false,
    cloudUser: {
        organizationId:null,
        joinedAsAdmin:null,
        isMember:null,
        organizationLicenseConsumed:null
    }
}
