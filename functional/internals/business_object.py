from dataclasses import dataclass


@dataclass
class BusinessObject:
    """
    Simulated Business Object, with a required "id" string, "name" string,
    and "value" int (defaulting to zero), and optional "description" string.
    """

    name: str
    value: int = 0
    description: str = None

    def init_from_raw(self, raw_object: dict) -> None:
        """
        Attempt to initialize fields from a raw JSON dictionary object
        """
        self.id = raw_object.get("id")
        self.name = raw_object.get("name")
        self.value = raw_object.get("value")
        self.description = raw_object.get("description")

    def validate(self) -> None:
        """
        Validate the data in this object, raising a ValueError to indicate problems
        """
        if not self.id:
            raise ValueError(
                "Validation Failed: ID is required! (must be a non-empty string)"
            )

        if not isinstance(self.id, str):
            raise ValueError("Validation Failed: ID is not a String!")

        if not self.name:
            raise ValueError(
                "Validation Failed: Name is required! (must be a non-empty string)"
            )

        if not isinstance(self.name, str):
            raise ValueError("Validation Failed: Name is not a String!")

        if self.value is None:
            raise ValueError(
                "Validation Failed: Value cannot be None (must be an integer)"
            )

        if self.description and not isinstance(self.description, str):
            raise ValueError("Validation Failed: Description is not a String!")
