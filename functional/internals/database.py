import time


class db:
    def execute(self, query):
        time.sleep(0.1)
        return True


def build_upsert_query(business_object):
    query = f"""
INSERT INTO business_objects (id, name, value, description)
VALUES ("{business_object.id}", "{business_object.name}",
"{business_object.value}", "{business_object.description}")
ON CONFLICT
    UPDATE business_objects SET
    name = "{business_object.name}",
    value = "{business_object.value}",
    description = "{business_object.description}"
    WHERE id="{business_object.id}"
"""
    return query
