class BadRequestError(Exception):
    pass

class SimulatedListRequest:
    def __init__(self, credentials: dict, page: int) -> dict:
        self.credentials = credentials
        self.page = page

    def get(self):
        try:
            return __LIST_RESPONSE_PAGES[self.page]
        except Exception:
            raise BadRequestError(f"Invalid Page Requested: {self.page}")


class SimulatedObjectRequest:
    def __init__(self, credentials, object_id):
        self.credentials = credentials
        self.object_id = object_id

    def get(self):
        response = {

        }
        return response


def build_api_list_request(credentials: dict, page: int) -> dict:
    return {
        "url": f"example.com/api/v2/business-objects?page={page}",
        "method": "GET",
        "headers": {
            "Account": credentials["api_key"],
            "Access Token": credentials["api_secret"],
        },
    }


def extract_list_objects(response: dict) -> dict:
    page = response.get("page")
    objects = page.get("objects")
    return objects


def extract_list_more_pages(response: dict) -> bool:
    more_pages_str = response.get("more_pages")
    if not more_pages_str:
        return False
    if "t" in more_pages_str.lower():
        return True
    return False


def build_api_object_request(credentials: dict, object_id: str):
    return {
        "url": f"example.com/api/v2/business-objects/{object_id}",
        "method": "GET",
        "headers": {
            "Account": credentials["api_key"],
            "Access Token": credentials["api_secret"],
        },
    }


def extract_object(response: dict) -> dict:
    raw_business_object = response.get["business-object"]
    return raw_business_object

__LIST_RESPONSE_PAGES = [
    {
        'page': 1,
        'has_more_pages': 'true',
        'objects': [
            '02701b6c-0142-4bcd-8a5b-04e5ccd1b87f',
            '615a7f0c-bb52-4190-80e0-bbecb05949d3',
            '3be51a28-6364-4c01-9a5d-d37b023aa205',
        ],
    },
    {
        'page': 2,
        'has_more_pages': 'true',
        'objects': [
            '856a67d2-92c7-4f7b-889f-d7387610787f',
            'd7902019-094d-44a2-ae37-e7fa6189cae3',
            '92c27ff7-042d-4df6-bb45-d5378a5c8966',
        ]
    },
    {
        'page': 3,
        'has_more_pages': 'false',
        'objects': [
            'fd32e066-d398-470a-8cc6-90791d82b028',
        ]
    }
]

__OBJECT_RESPONSES = {
    '02701b6c-0142-4bcd-8a5b-04e5ccd1b87f': {
        'id': '02701b6c-0142-4bcd-8a5b-04e5ccd1b87f',
        'name': 'Free Trial',
        'value': '0',
        'description': '(30 day trial account, limit 1 per account)',
    },
    '615a7f0c-bb52-4190-80e0-bbecb05949d3': {
        'id': '615a7f0c-bb52-4190-80e0-bbecb05949d3',
        'name': 'Extended Trial',
    },
    '3be51a28-6364-4c01-9a5d-d37b023aa205': {
        'id': '3be51a28-6364-4c01-9a5d-d37b023aa205',
        'name': 'Personal Edition',
        'value': '20',
        'description': 'Starter personal license',
    },
    '856a67d2-92c7-4f7b-889f-d7387610787f': {
        'id': '856a67d2-92c7-4f7b-889f-d7387610787f',
        'name': 'Professional Edition',
        'value': '35',
        'description': 'Fully featured personal license',
    },
    'd7902019-094d-44a2-ae37-e7fa6189cae3': {
        'id': 'd7902019-094d-44a2-ae37-e7fa6189cae3',
        'name': 'Enterprise Edition (starter)',
        'value': '50',
        'description': 'Starter Enterprise Edition, up to 100 seats',
    },
    '92c27ff7-042d-4df6-bb45-d5378a5c8966': {
        'id': '92c27ff7-042d-4df6-bb45-d5378a5c8966',
        'name': 'Enterprise Edition (level 1 bulk discount)',
        'value': '45',
        'description': 'Enterprise Edition Bulk Discount for 500 - 999 seats',
    },
    'fd32e066-d398-470a-8cc6-90791d82b028': {
        'id': '92c27ff7-042d-4df6-bb45-d5378a5c8966',
        'name': 'Enterprise Edition (level 2 bulk discount)',
        'value': '40',
        'description': 'Enterprise Edition Bulk Discount for 1000+ seats',
    }
}