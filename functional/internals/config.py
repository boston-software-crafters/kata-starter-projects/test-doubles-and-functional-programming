def load_credentials():
    """
    Simulates getting credentials from some kind of secure storage
    (pretty much just works, but is considered a side effect)
    """
    return {
        "api_key": "foo",
        "api_secret": "bar",
    }
