# Test Doubles and" Mocking"

Terminology around Testing, and especially around Mocking, is extremely inconsistent! Naming stuff is hard, and every new language and testing library is a new chance to mis-use some words. But having clear vocabulary is important, so let's try to set some ground rules:

The phrase "Test Double" can be used to describe ["any case where you replace a Production object, for Testing Purposes."](https://martinfowler.com/bliki/TestDouble.html#:~:text=Test%20Double%20is%20a%20generic,production%20object%20for%20testing%20purposes.), and in his Blog post, Martin Fowler attempts to itemize some more specific types of them as well:

 * *Dummy* objects are passed around, but never actually used (i.e. they are just used to satisfy the requirements for calling a function or method).

 * *Fake* objects actually have working implementations, but usually take some shortcut which makes them not suitable for Production (an InMemoryTestDatabase is a good example).

 * *Stubs* provide canned answers to calls made during the test, usually not responding at all to anything outside what was needed for a specific test.

 * *Spies* are stubs that also record some information based on how they were called. One form of this might be an email service that records how many messages it was sent.

 * *Mocks* are pre-programmed with expectations which form a specification of the calls they are expected to receive. They can throw an exception if they receive a call they don't expect and are checked during verification to ensure they got all the calls they were expecting.

Depending on your preferred Stack and and Testing Framework, you should hopefully at least have some version of the above!











@Bo Rodriguez
@Rachna
@trevor-welch
@Dan Dickson
@kevin-cocco